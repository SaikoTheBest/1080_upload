﻿using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;

namespace _1080_Upload
{
	internal static class JsonValidator
	{
		private const string ResourcesPath = "Resources/";
		private static readonly JSchema Schema;

		static JsonValidator()
		{
			Schema = JSchema.Parse(File.ReadAllText($"{ResourcesPath}schema.json"));
		}

		internal static bool IsTestValid(string json)
		{
			try
			{
				var obj = JObject.Parse(json);
				return obj.IsValid(Schema);
			}
			catch (JsonReaderException)
			{
				return false;
			}
		}
	}
}