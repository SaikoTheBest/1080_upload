﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace _1080_Upload
{
	internal class Program
	{
		private const string TestsPath = "Resources/Tests/";
		private static HttpClient client;

		private static void Main()
		{
			InitClient();
			UploadTests();
			ReadKey();
		}

		static void InitClient()
		{
			client = new HttpClient
			{
				BaseAddress = new Uri("http://176.9.22.172:8101/")
			};
			client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",
				"CfDJ8JrkTEK_2sJElNcefn6yiiHo6wGiVgoueCZBaEXxRib8-4NUPiSl84AtvXN473mFvV1cUQnMP-HQhDRnz6Yt-gkqdd2PYam7T9kjzZUGlB3uc71CIywgSVYs7L89Fh3csfvrauG6SCStJP9Cxr7AH3K-GUeswq5i9hX5iVJFWyYMD3xAThFSD86J46ocXjAPjAKkLfsSA9ucw6QyvkxmaxE538-wlRUljd3SAb-iBP-XsozvasfWKoQV2Euqo3kmxB-Gmb1uuQVwU7-L4Vou2xviu2Lrh6ltz84bMb6TSgg_uWOCxPOHiRqBDuuArNpOSZB-htjO7re4hJtpHczNL2AhKMYAeKLDvJ3GjUBaHnTW52eSbL-6eAJW6Qg-keUkm6vfpeArTYTW6Kh3CbZciJ8eXbQTet0zAOkzSPtn_TQ6dB9dUsqISdbI06wWMiMYNqynAAm1WfsAhz9EP-XVJbIwjoy7tx8PepO8FKUvsixelHG0wxhGZ9TYImiFQMKIPIM4qRUKl_Ozo4Pj9iOxMdzd1IRV3si7hmHcR1_hcQoh");
		}

		private static void UploadTests()
		{
			var files = GetFilesRecursively(TestsPath);
			foreach (var file in files)
			{
				var json = File.ReadAllText(file);
				if (JsonValidator.IsTestValid(json))
				{
					SendTest(json)
						.ContinueWith(task =>
						{
							if (task.IsFaulted)
							{
								WriteLine($"{file}. Not send. Api is not available");
							}
							else
							{
								var response = task.Result;
								if (!response.IsSuccessStatusCode)
								{
									WriteLine($"{file}. Response with error code {response.StatusCode}");
								}
							}
						});
				}
				else
				{
					WriteLine($"{file}. Json is not valid");
				}
			}
		}

		private static IEnumerable<string> GetFilesRecursively(string path)
		{
			foreach (var file in Directory.GetFiles(path))
			{
				yield return file;
			}
			foreach (var directory in Directory.GetDirectories(path))
			{
				foreach (var file in GetFilesRecursively(directory))
					yield return file;
			}
		}

		private static async Task<HttpResponseMessage> SendTest(string json)
		{
			var request = new HttpRequestMessage(HttpMethod.Put, "tests/edit")
			{
				Content = new StringContent(json, Encoding.UTF8, "application/json")
			};
			return await client.SendAsync(request);
		}
	}
}